# Viseras
## ¿Cuál o cuáles son los materiales válidos para fabricar viseras?
Recomendamos usar PLA por su facilidad de impresión. No obstante, puedes usar otros como ABS, PETG, etc..

##Solamente tengo ABS. ¿Puedo imprimir las piezas con este material?
Si, pero si eres novel en impresión 3D quizas es mejor usar PLA

## ¿Dónde puedo encontrar los archivos para imprimir viseras y piezas del respirador?
En este repositorio :)

## ¿Qué precauciones debemos tomar los que fabriquemos algo asumiendo que podamos contagiar? ¿Fabricación con guantes? ¿Se deben esterilizar/desinfectar los antisalpicaduras, prototipos, etc?
Es muy importante tomar las precauciones necesarias para no actuar como vector. Por ello, cuando prepares el material para entregar deberias desinfectarlo con EtOh 70 o lejia 5%. Una vez seco, metelo dentro de una bolsa zp o similar. En todo momento deberias usar guantes y mascarilla.

## ¿Qué grosor deben tener las láminas de PVC para funcionar con los diseños impresos en 3D?
Idealmente, 0.5mm de espesor y tamaño DINA4 usado en modo horizontal
Si no encuentas PVC, puedes usar PETG. Otra opción es usar laminas de acetato.

## Me gustaría ayudar creando viseras utilizando grapas/gomas. ¿Cómo podría hacerlo? 
